// 页面路径：store/index.js 
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex); //vue的插件机制

//Vuex.Store 构造器选项
const store = new Vuex.Store({
	state: { //存放状态
		userInfo: null,
		baseUrl: "http://101.35.249.138/back",
		logo: require("@/static/logo.png")
	},
	getters: {

	},
	mutations: {
		setUserInfo(state, userInfo) {
			state.userInfo = userInfo
			if (userInfo)
				state.userInfo.img = state.baseUrl + userInfo.img
		}
	}
})


export default store
