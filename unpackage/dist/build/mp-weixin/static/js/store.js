// 页面路径：store/index.js 
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex); //vue的插件机制


const TOKEN_VO = "tokenVo"

const USER_INFO = "userInfo"

const LIGHT_THEME = "light"

const DARK_THEME = "dark"

//Vuex.Store 构造器选项
const store = new Vuex.Store({
	state: {
		token: null,
		expireTime: -1,
		userInfo: null,
		baseUrl: "http://101.35.249.138/back",
		logo: require("@/static/logo.png"),
		// 列表加载中
		LOADING: 1,
		// 页面加载中
		PAGE_LOADING: 4,
		// 请求成功
		LOAD_SUCCESS: 2,
		// 加载成功但是没有数据时
		LOAD_EMPTY: 3,
		// 列表加载更多时失败
		LOAD_MORE_ERROR: -3,
		// 页面或列表初始化时网络异常
		LOAD_NET_ERROR: -2,
		// 页面或列表初始化时后台异常
		LOAD_SERVICE_ERROR: -1,
		// 初始化值
		LOAD_INIT: 0,

		theme: 'light',

		bgColor: '#ffffff',

		fontColor: "#000000",

		tintBgColor: "#f8f8f8",

		darkBgColor: "#000000",

		darkFontColor: "#ffffff",

		dackTintBgColor: "#080808",

		homeActive: 'shelf'
	},
	getters: {
		bgColor(state) {
			return state.theme === LIGHT_THEME ? state.bgColor : state.darkBgColor
		},
		fontColor(state) {
			return state.theme === LIGHT_THEME ? state.fontColor : state.darkFontColor
		},
		tintBgColor(state) {
			return state.theme === LIGHT_THEME ? state.tintBgColor : state.dackTintBgColor
		},
		userInfo(state) {
			if (state.userInfo) {
				return state.userInfo
			} else {
				try {
					const userInfo = uni.getStorageSync(USER_INFO);
					if (userInfo) {
						state.userInfo = userInfo
						return state.userInfo
					}
				} catch (e) {
					// error
				}
			}
		},
		token(state) {
			if (state.token) {
				return state.token
			} else {
				try {
					const tokenVo = uni.getStorageSync(TOKEN_VO);
					if (tokenVo) {
						state.token = tokenVo.token
						state.expireTime = tokenVo.expireTime
						return state.token
					}
				} catch (e) {
					// error
				}
			}
		},
		expireTime(state) {
			if (state.expireTime > -1) {
				return state.expireTime
			} else {
				try {
					const tokenVo = uni.getStorageSync(TOKEN_VO);
					if (tokenVo) {
						state.token = tokenVo.token
						state.expireTime = tokenVo.expireTime
						return state.expireTime
					}
				} catch (e) {
					// error
				}
			}
		},
		tokenVo(state) {
			if (state.token) {
				return {
					token: state.token,
					expireTime: state.expireTime
				}
			} else {
				try {
					const tokenVo = uni.getStorageSync(TOKEN_VO);
					if (tokenVo) {
						return tokenVo
					}
				} catch (e) {
					// error
				}
			}
		},
		isLogin(state) {
			if (state.expireTime === -1) {
				try {
					const tokenVo = uni.getStorageSync(TOKEN_VO);
					if (tokenVo) {
						state.token = tokenVo.token
						state.expireTime = tokenVo.expireTime
					}
				} catch (e) {
					// error
				}
			}
			return state.expireTime > new Date().getTime()
		}
	},
	mutations: {
		CHANGE_ACTIVE(state, active) {
			state.homeActive = active
		},
		SET_THEME(state, theme) {
			state.theme = theme
		},
		CHANGE_THEME(state) {
			if (state.theme === LIGHT_THEME) {
				state.theme = DARK_THEME
			} else {
				state.theme = LIGHT_THEME
			}
		},
		LOGOUT(state) {
			try {
				uni.removeStorageSync(TOKEN_VO);
				uni.removeStorageSync(USER_INFO);
			} catch (e) {
				// error
			}
			state.userInfo = null
			state.token = null
			state.expireTime = -1
		},
		SET_USER_INFO(state, userInfo) {
			state.userInfo = userInfo
			if (userInfo)
				state.userInfo.img = state.baseUrl + userInfo.img
			try {
				uni.setStorageSync(USER_INFO, state.userInfo);
			} catch (e) {
				// error
			}
		},
		LOGIN(state, tokenVo) {
			console.log("===store login===")
			state.token = tokenVo.token
			state.expireTime = tokenVo.expireTime
			state.userInfo = tokenVo.user

			if (tokenVo.user.img)
				state.userInfo.img = state.baseUrl + tokenVo.user.img
			try {
				uni.setStorageSync(TOKEN_VO, {
					token: tokenVo.token,
					expireTime: tokenVo.expireTime
				})
				uni.setStorageSync(USER_INFO, tokenVo.user)
			} catch (e) {
				console.error("===store login===")
			}
		},
		REFRESH_TOKEN(state, tokenVo) {
			state.token = tokenVo.token
			state.expireTime = tokenVo.expireTime
			try {
				uni.setStorageSync(TOKEN_VO, {
					token: tokenVo.token,
					expireTime: tokenVo.expireTime
				});
			} catch (e) {
				// error
			}
		},
	},
	actions: {

	}
})


export default store