import store from '@/static/js/store.js'

class Http {
	constructor(arg) {
		// 弃用
		this.token = null
		this.expireTime = 0
		this.baseUrl = "http://101.35.249.138/back"
		// this.baseUrl = "http://127.0.0.1"
		// this.baseUrl = "http://192.168.43.254"
	}

	isLogin() {
		let expire = this.getExpireTime()
		return expire > new Date().getTime()
	}

	removeTokenAndExpireTime() {
		try {
			uni.removeStorageSync('token');
			uni.removeStorageSync('expireTime');
			this.token = null
			this.expireTime = 0
		} catch (e) {
			// error
		}
	}
	setToken(token) {
		this.token = token
		uni.setStorage({
			key: 'token',
			data: token,
			success: function() {
				console.log('将 token 保存到本地');
			}
		});
	}
	setExpireTime(expireTime) {
		this.expireTime = expireTime
		uni.setStorage({
			key: 'expireTime',
			data: expireTime,
			success: function() {
				console.log('将 token expireTime 保存到本地');
			}
		})
	}
	getExpireTime() {
		try {
			if (this.expireTime !== 0) {
				return this.expireTime
			}
			const value = uni.getStorageSync('expireTime');

			console.info('expireTime 获取成功');

			if (value) {
				this.expireTime = value
				return value
			} else {
				return 0
			}
		} catch (e) {
			// error
			console.error('expireTime 获取失败', e);
		}
	}
	getToken() {
		try {
			if (this.token) {
				return this.token
			}
			const value = uni.getStorageSync('token');

			console.info('token 获取成功');

			if (value) {
				this.token = value
				return value
			} else {
				return null
			}
		} catch (e) {
			// error
			console.error('token 获取失败', e);
		}
	}

	getBaseUrl() {
		return this.baseUrl
	}


	get(url, param = null, header = null) {

		let isLogin = store.getters.isLogin
		let token = store.getters.token
		let baseUrl = store.state.baseUrl

		if (isLogin) {
			if (param != null) {
				param.token = token
			} else {
				param = {}
				param.token = token
			}

			if (header != null) {
				header.token = token
			} else {
				header = {}
				header.token = token
			}
		}
		return new Promise((resolve, reject) => {
			uni.request({
				url: baseUrl + url,
				data: param,
				header: header,
				dataType: "json",
				success(res) {
					let resData = res.data
					if (typeof resData == 'object') {
						resolve(resData)
					} else if (typeof resData == 'string') {
						resolve(JSON.parse(resData + ""))
					}
				},
				fail(error) {
					reject(error)
				},
				complete() {

				}
			});
		})
	}

	post(url, data = null, header = null) {
		let isLogin = store.getters.isLogin
		let token = store.getters.token
		let baseUrl = store.state.baseUrl

		if (isLogin) {
			if (data != null) {
				data.token = token
			} else {
				data = {}
				data.token = token
			}
			if (header !== null) {
				header.token = token
			} else {
				header = {}
				header.token = token
			}
		}
		return new Promise((resolve, reject) => {
			uni.request({
				url: baseUrl + url,
				data: data,
				method: "POST",
				header: header,
				dataType: "json",
				success(res) {
					let resData = res.data
					if (typeof resData === 'object') {
						resolve(res.data)
					} else if (typeof resData === 'string') {
						resolve(JSON.parse(resData + ""))
					}

				},
				fail(res) {
					reject(res)
				},
				complete() {

				}
			});
		})
	}
}

let http = new Http()




/**
 * 弃用,改用vuex取代
 */
class GlobalData {
	constructor(arg) {
		this.userInfo = ""
	}
	setUserInfo(userInfo) {
		this.userInfo = userInfo
		if (userInfo)
			this.userInfo.img = http.baseUrl + userInfo.img
	}
	getUserInfo() {
		return this.userInfo
	}
}

// https://uniapp.dcloud.io/tutorial/syntax-css.html#css-%E5%8F%98%E9%87%8F
class System {
	constructor(arg) {
		this.__window_top = 0
		this.__status_bar_height = 0
		this.__window_bottom = 0
		// android ios h5 mini-weixin mini-baidu
		this.endpoint = "android"
	}
	getWindowTop() {
		if (this.endpoint === "android" || this.endpoint === "ios" || this.endpoint.startsWith("mini")) {
			return 0
		} else if (this.endpoint === "h5") {
			// NavigationBar 的高度 一般为 44 px
			return 44
		}
	}
	getWindowBottom() {
		if (this.endpoint === "android" || this.endpoint === "ios" ||
			this.endpoint.startsWith("mini")) {
			return 0
		} else if (this.endpoint === "h5") {
			// TabBar  的高度 一般为 50 px
			return 50
		}
	}
	getStatusBar() {
		if (this.endpoint.startsWith("mini")) {
			return 25
		} else if (this.endpoint === "h5") {
			return 0
		} else if (this.endpoint === "android" || this.endpoint === "ios") {
			return plus.navigator.getStatusbarHeight()
		}
	}
	getNavBar() {
		// 根据是否为自定义nav-bar判定
		return 44
	}
	isMini() {
		return this.endpoint.startsWith("mini")
	}
}


const timeList = date => {
	const year = date.getFullYear()
	const month = date.getMonth() + 1
	const day = date.getDate()
	const hour = date.getHours()
	const minute = date.getMinutes()
	const second = date.getSeconds()
	let list = [year, month, day, hour, minute, second]
	return list
}

// time 评论发表时间(毫秒)
const formatTime = time => {

	let date = new Date(time)
	let list = timeList(date)


	let currentDate = new Date()
	let currentList = timeList(currentDate)

	let second = parseInt(time / 1000)
	let currentSecond = parseInt(currentDate.getTime() / 1000)
	// 单位:秒
	let distance = currentSecond - second

	if (distance > 0 && distance < 60) {
		return `${distance}秒前`
	}
	// 分
	distance = parseInt(distance / 60)

	if (distance > 0 && distance < 60) {
		return `${distance}分前`
	}

	// 小时
	distance = parseInt(distance / 60)

	if (distance > 0 && distance < 24) {
		return `${distance}小时前`
	}


	// 天
	distance = parseInt(distance / 24)

	if (distance > 0 && distance < 7) {
		return `${distance}天前`
	} else if (distance == 7) {
		return `1星期前`
	}

	if (distance > 365 && list[0] < currentList[0]) {
		return `${list[0]}-${list[1]}-${list[2]}`
	} else {
		return `${list[1]}-${list[2]}`
	}
}

const formatNumber = num => {
	if (num < 10000) {
		return num
	}
	num = num / 10000

	if (num < 10000) {
		return num.toFixed(1) + "万"
	}

	num = num / 10000

	if (num < 10000) {
		return num.toFixed(1) + "亿"
	}

}


const imgExchange = function(img) {
	if (img) {
		img = img.replace(/(\r\n)|(\n)|( )/g, '');
	}
	// if (img && img.indexOf('yuewen') > 0) {
	// 	if (img.indexOf("jpeg") < 0) {
	// 		img = img + ".jpeg";
	// 	}
	// }
	return img
}



export default {
	http,
	globalData: new GlobalData(),
	formatTime,
	imgExchange,
	formatNumber,
	system: new System()
}
