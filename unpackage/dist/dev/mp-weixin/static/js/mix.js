//minxins.js 文件

import {
	mapState,
	mapMutations,
	mapGetters
} from 'vuex' //引入mapGetters

import utils from '@/static/js/utils.js'

export default {
	data() {
		return {}
	},
	mounted() {
		// DOM 还没有更新
		this.$nextTick(function() {
			this.setStatusBar()
		})
	},
	watch: {
		theme(newV, oldV) {
			this.setStatusBar()
		}
	},
	onShow() {
		this.setStatusBar()
	},

	computed: {
		...mapState(['LOADING', 'LOAD_SUCCESS', 'LOAD_NET_ERROR', 'LOAD_SERVICE_ERROR',
			'LOAD_INIT', 'LOAD_EMPTY', 'PAGE_LOADING', 'logo', 'theme', 'bgColor', 'fontColor'
		]),

		...mapGetters(['userInfo', 'isLogin', 'tokenVo'])
	},
	methods: {
		...mapMutations(['CHANGE_THEME', 'LOGOUT']),
		setStatusBar() {
			// #ifdef APP-VUE
			if (this.theme === 'dark' && utils.system.endpoint === 'android') {
				plus.navigator.setStatusBarStyle('light');
				plus.navigator.setStatusBarBackground("#000")
			} else if (this.theme === 'light' && utils.system.endpoint === 'android') {
				plus.navigator.setStatusBarStyle('dark');
				plus.navigator.setStatusBarBackground("#fff")
			}
			// #endif
			
			
		},
		formatTime(time) {
			return utils.formatTime(time)
		}
	}
}
