//minxins.js 文件

import {
	mapState,
	mapMutations,
	mapGetters
} from 'vuex' //引入mapGetters

export default {
	data() {
		return {

		}
	},
	computed: {
		...mapState(['LOADING', 'LOAD_SUCCESS', 'LOAD_NET_ERROR', 'LOAD_SERVICE_ERROR',
			'LOAD_INIT', 'LOAD_EMPTY', 'PAGE_LOADING', 'logo', 'theme'
		]),

		...mapGetters(['userInfo', 'isLogin'])
	},
	methods: {
		...mapMutations(['CHANGE_THEME'])
	}
}
