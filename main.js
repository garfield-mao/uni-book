import Vue from 'vue'
import App from './App'
import store from './static/js/store.js'
import utils from  './static/js/utils.js'

Vue.prototype.$store = store

Vue.prototype.$http = utils.http

import uView from "uview-ui";

Vue.use(uView);

// uni.$u.config.unit = 'rpx'

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	store,
	...App
})
app.$mount()
