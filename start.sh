docker stop nginx2
docker rm nginx2
rm -rf /var/jenkins_home/images/nginx/h5/
mkdir /var/jenkins_home/images/nginx/h5
cp -r unpackage/dist/build/web/* /var/jenkins_home/images/nginx/h5/
docker run -d --name nginx2 --link book  \
-p 80:80 \
-v /home/lighthouse/jenkins/images/nginx:/etc/nginx/ \
-v /home/lighthouse/jenkins/images/nginx/log/:/var/log/nginx \
nginx
