export default {

	data() {
		return {
			// 17K
			kSwiper: [{
				image: 'https://img.17k.com/cmsimg/recommend/2022/03/1647223560.5136.jpg',
				title: '网游：精灵世纪，王者归来',
			}, {
				image: 'https://img.17k.com/cmsimg/recommend/2022/03/1646621207.7936.jpg',
				title: '我有一剑，可斩星，碎月'
			}, {
				image: 'https://img.17k.com/cmsimg/recommend/2021/12/1639967393.5629.jpg',
				title: '师傅说我天生吃阴饭'
			}],

			// 最新入库
			kNewList: null,

			kClick: null,
		};
	},
	onLoad() {
		this.k17Show()
	},
	onShow() {
		this.k17Show()
	},
	methods: {
		k17Show() {
			if (!this.kNewList || this.kNewList.length === 0)
				this.loadNewList("17K小说网").then(res => {
					if (res.status === 200) {
						this.kNewList = res.data.list
					}
				})
			if (!this.kClick || this.kClick.length === 0)
				this.rankList("17K小说网", "kClick")
		}
	}
}
