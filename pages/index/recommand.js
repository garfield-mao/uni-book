export default {

	data() {
		return {
			//推荐
			list: [],
			commandRefresing: false,
		};
	},
	onLoad() {
		
	},
	onShow() {
		if (!this.list || this.list.length === 0)
			this.$refs.paging.reload()
	},

	methods: {
		queryList(pageNo, pageSize) {
			//这里的请求只是演示，请替换成自己的项目的网络请求，
			// 并在网络请求回调中通过this.$refs.paging.complete(请求回来的数组)将请求结果传给z-paging
			let page = 1
			let size = 5
			let date = new Date()
			let year = date.getFullYear()
			let month = date.getMonth()
			let day = date.getDate()

			this.$http.get(`/rank/book/click`, {
					page: page,
					pageSize: size,
					year: year,
					month: month,
					day: day,
					isAsc: false
				}).then(res => {
					// console.log(res)
					if (res.status === 200) {
						this.$refs.paging.complete(res.data.list);
					} else {
						this.$refs.paging.complete(false);
					}

				})
				.catch(e => {
					this.$refs.paging.complete(false)
				})
				.finally(e => {
					if (this.commandRefresing)
						this.commandRefresing = false
				})
		},
		// 推荐下拉刷新
		onCommandRefresh() {
			this.commandRefresing = true
			this.recommand()
		}, // 推荐list加载
		recommand() {
			let page = 1
			let pageSize = 5
			let date = new Date()
			let year = date.getFullYear()
			let month = date.getMonth()
			let day = date.getDate()

			this.$http.get(`/rank/book/click`, {
				page: page,
				pageSize: pageSize,
				year: year,
				month: month,
				day: day,
				isAsc: false
			}).then(res => {
				// console.log(res)
				if (res.status == 200) {
					this.list = res.data.list
				}
			}).finally(e => {
				if (this.commandRefresing)
					this.commandRefresing = false
			})
		},
	}
}
