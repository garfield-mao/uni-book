export default {

	data() {
		return {
			// 纵横
			zSwiper: [{
				image: 'http://static.zongheng.com/upload/recommend/78/f7/78f78f2ce0c438daaf05e2718408cd4f.jpeg',
				title: '末世吞噬者',
			}, {
				image: 'http://static.zongheng.com/upload/recommend/d8/e6/d8e6245782f8511ada07239941cb19f0.png',
				title: '三世放飞芳菲你'
			}, {
				image: 'http://static.zongheng.com/upload/recommend/f8/24/f8245ff11bbbd191e7c4fe57211b50d8.jpeg',
				title: '大炎不良人'
			}],

			// 最新入库
			zNewList: null,

			zClick: null,

		};
	},
	onLoad() {
		this.zhShow()

	},
	
	onShow() {
		this.zhShow()
	},
	methods: {
		zhShow() {
			if (!this.zNewList || this.zNewList.length === 0)
				this.loadNewList("纵横中文网").then(res => {
					if (res.status === 200) {
						this.zNewList = res.data.list
					}
				})
			if (!this.zClick || this.zClick.length === 0)
				this.rankList("纵横中文网", "zClick")
		}
	}
}
