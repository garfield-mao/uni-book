export default {

	data() {
		return {
			// 起点
			qSwiper: [{
				image: 'https://bossaudioandcomic-1252317822.image.myqcloud.com/activity/document/223638d0f786bfe2d7caf71ac480563e.jpg',
				title: '重生三国从养鸡开始',
			}, {
				image: 'https://bossaudioandcomic-1252317822.image.myqcloud.com/activity/document/2dce5b2088b14619fcff97b30e9d1ccb.jpg',
				title: '神话：天罡地煞'
			}, {
				image: 'https://bossaudioandcomic-1252317822.image.myqcloud.com/activity/document/13e00d56cb24faad0baa64f4d3dd67e9.jpg',
				title: '大理寺卿的日常事'
			}],

			// 最新入库
			qNewList: null,

			// 最热
			qClick: null,
		};
	},

	onLoad() {
		this.qdShow()
	},
	
	onShow(){
		this.qdShow()
	},

	methods: {
		qdShow() {
			if (!this.qNewList || this.qNewList.length === 0)
				this.loadNewList("起点中文网").then(res => {
					if (res.status === 200) {
						this.qNewList = res.data.list
					}
				})
			if (!this.qClick || this.qClick.length === 0)
				this.rankList("起点中文网", "qClick")
		}
	}
}
