export default {
	data() {
		return {
			bgOpacity: 0,
			bgHeight: 45 * 1.5
		}
	},
	onPageScroll(e) {
		let temp = e.scrollTop / this.bgHeight
		this.bgOpacity = temp > 1 ? 1 : temp
	}
}
